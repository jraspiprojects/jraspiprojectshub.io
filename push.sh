#!/bin/bash
git add --all
if [ "$0" != "" ]; then
    echo "Commiting..."
    git commit -m "$0"
else
    echo "Commit name is blank!"
    echo "Exiting..."
    exit
fi
git push -u origin master

